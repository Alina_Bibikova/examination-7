import React from "react";

const OrderDetails = props => (
    <div className='OrderDetails'>
        <span className="details-name">{props.name}</span>
        <span className="details-count">X {props.count}</span>
        <button className="removeClick" onRemove={() => props.onRemove(props.id)}>x</button>
    </div>
);

export default OrderDetails;