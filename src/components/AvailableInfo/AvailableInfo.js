import React from 'react';

const availablelInfo = props => (

    <div className="avail-wrap">
        <div className="avail-name">
            <div><span>{props.name}</span></div>
            <div> <span>Price: {props.price} c</span></div>
            <button className="addClick" addClick={() => props.addClick(props.id)}>+</button>
        </div>
        <div>
            {props.children}
        </div>
    </div>
);

export default availablelInfo;