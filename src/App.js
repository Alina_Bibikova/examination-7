import React, { Component } from 'react';
import AvailableInfo from "./components/AvailableInfo/AvailableInfo";
import OrderDetails from "./components/OrderDetails/OrderDetails";
import AddedInfo from "./components/AddedInfo/AddedInfo"

import './App.css';

class App extends Component {

    state = {
        available: [
            {id: 1, name: 'Hamburger', price: 80, count: 0},
            {id: 2, name: 'Cheeseburger', price: 90, count: 0},
            {id: 3, name: 'Fries', price: 45, count: 0},
            {id: 4, name: 'Coffee', price: 70, count: 0},
            {id: 5, name: 'Tea', price: 50, count: 0},
            {id: 6, name: 'Cola', price: 40, count: 0},

        ],

        totalPrice: 0
    };

    showInfo = (details) => {
        const result = [];

        for(let i = 0; i < details.length; i++){
            for (let j = 0; j < details[i].count; j++){
                result.push(<AddedInfo name={details[i].name} key={'' + i + '' + j}/>)
            }
        }
        return result;
    };

    addClick = (details) => {
        const detailsInfo = [...this.state.available];

        for (let i = 0; i < detailsInfo.length; i++) {
            if (detailsInfo[i].name === details) {
                detailsInfo[i].count++;
            }
        }

        this.setState({available: detailsInfo}, this.getTotalPrice());
    };

    removeClick = (details) => {
        const detailsInfo = [...this.state.available];

        for (let i = 0; i < detailsInfo.length; i++) {
            if (detailsInfo[i].name === details) {
                detailsInfo[i].count--;

                if (detailsInfo[i].count < 0) {
                    detailsInfo[i].count = 0;
                }
            }
        }

        this.setState({available: detailsInfo}, this.getTotalPrice);
    };

    getTotalPrice = () => {
        const detailsInfo = [...this.state.available];

        let totalPrice = 0;

        for (let i = 0; i < detailsInfo.length; i++) {
            if (detailsInfo[i].count) {
                let detailsPrice = detailsInfo[i].count * detailsInfo[i].price;
                totalPrice += detailsPrice;
            }
        }

        this.setState({totalPrice});
    };

    render() {
        let DetailsInfo = null;

        if (DetailsInfo !== "") {
            DetailsInfo = (
                <div className="constructor-right">
                    <h2>Order Items</h2>
                    <OrderDetails
                        key={this.state.key}
                        name={this.name}
                        onRemove={() => this.removeClick()}
                    >
                        {this.showInfo(this.state.available)}
                    </OrderDetails>
                    <p>Total Price: {this.state.totalPrice} KGS</p>
                </div>
            )
        } else {
            DetailsInfo = (
                <div className="constructor-right">
                    <p>Order is empty! Please add some items!</p>
                </div>
            )
        }

        return (
            <div className="container">
                <div className="constructor">
                    <div className="constructor-left">
                        <h2>Add Items</h2>
                        {this.state.available.map((details) => (
                            <AvailableInfo
                                key={details.id}
                                name={details.name}
                                addClick={() => this.addClick()}
                                price={details.price}
                            />
                        ))}
                    </div>
                    {DetailsInfo}
                </div>
            </div>
        );
    }
}

export default App;
